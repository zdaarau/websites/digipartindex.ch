// Put custom JavaScript code in this file
"use strict";

// slick.js logo slider config
$('.logo-slider').slick({
  arrows: false,
  slidesToShow: 3,
  responsive: [
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2000,
        centerMode: false,
        dots: true,
        slidesToShow: 1
      }
    }
  ]
});
