"use strict";
const allowedIds = ['name', 'email', 'subject', 'body'];
const emailLink = document.querySelector(".address-block span.cloaked-e-mail");
const emailLinkParams = new URLSearchParams();

(new URL(window.location.href)).searchParams.forEach((value, key) => {
  
  if (allowedIds.includes(key)) {
    // set default value of element with ID matching URL param
    document.getElementById(key).defaultValue = value;
    // append URL param to e-mail address
    emailLinkParams.append(key, value);
  }
})

// append URL params to cloaked contact block e-mail address
//
// NOTES:
// - `URLSearchParams` has no `size` property yet, cf. https://github.com/whatwg/url/issues/163 and https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams
// - for `mailto`-links to work with common e-mail clients like Thunderbird, spaces must be encoded as `%20`, not `+`; cf.
//   https://en.wikipedia.org/wiki/Percent-encoding#The_application/x-www-form-urlencoded_type and https://github.com/medialize/URI.js/issues/74
if (!emailLinkParams.entries().next().done) {
  emailLink.setAttribute("data-params", emailLinkParams.toString().replaceAll('+', '%20').split('').reverse().join(''));
}
