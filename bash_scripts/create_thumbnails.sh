#!/bin/bash

# ensure ImageMagick's `convert` tool is available
# on macOS, it's available via brew: https://formulae.brew.sh/formula/imagemagick#default
if ! command -v convert >/dev/null ; then
  echo "ImageMagick's \`convert\` tool not found but required. On macOS you can install it via Homebrew, see https://formulae.brew.sh/formula/imagemagick#default"
  exit 1
fi

for pdf in ../static/docs/DigiPartIndex*.pdf ; do

  filename=`basename $pdf .pdf`
  
  # create thumbnail with drop shadow
  convert -thumbnail x640 -background white -flatten $pdf[0] temp.png
  convert -alpha off temp.png temp.png
  convert temp.png \( -clone 0 -background gray -shadow 80x3+20+20 \) -reverse -background none -layers merge +repage temp.png
  
  ## optimize thumbnail filesize
  if command -v optipng >/dev/null ; then
    optipng -quiet temp.png
  fi
  if command -v pngquant >/dev/null ; then
    pngquant --speed 1 --strip --ext .png --force temp.png
  fi
  
  # move thumbnail
  mv --force temp.png "../static/images/thumbnails/${filename}.png"
  
done
