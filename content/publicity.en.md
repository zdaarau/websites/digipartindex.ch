---
title: "Publicity"
description: "Publications, events held and media coverage concerning the DigiPartIndex"
bg_image: "images/background/unsplash-6M9jjeZjscE-edited-2048w.jpg"
layout: "publicity"
slug: "publicity"
draft: false
menu:
  main:
    name: "Publicity"
    weight: 6
  footer:
    name: "Publicity"
    weight: 6
---
