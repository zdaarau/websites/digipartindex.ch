+++
title = "Domande frequenti"
subtitle = ""
description = "Domande e risposte sul DigiPartIndex"
bg_image = "images/background/unsplash-8xAA0f9yQnE-edited-2048w"
layout = "faq"
draft = true
[menu.main]
  name = "Domande frequenti"
  weight = 5
[menu.footer]
  name = "Domande frequenti"
  weight = 5
+++

#### 1. Chi finanzia questo progetto?

...

#### 2. Come vengono raccolti i dati?

...

#### 3. Ci sono altri indici nel campo della partecipazione politica digitale?

...
