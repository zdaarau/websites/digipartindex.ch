---
title: "Données"
description: "Les données et les organigrammes à la base du DigiPartIndex"
subtitle: ""
bg_image: "images/background/unsplash-trYl7JYATH0-edited-2048w"
layout: "data"
draft: false
menu:
  main:
    name: "Données"
    weight: 5
  footer:
    name: "Données"
    weight: 5

################################ Datasets ################################
datasets:
  enable : true
  items:
  - enable: true
    head : ""
    title: "Ensemble de données"
    content : "_XLSX fichier_"
    link : "/download/DigiPartIndex.xlsx"
    bullets:
    - "Contient 2 tableaux :"
    - "Ensemble de données plus métadonnées"

  - enable: true
    head : ""
    title: "Organigrammes "
    content : "_PDF fichier_"
    link : "/download/Flow_Charts_Data_Collection.pdf"
    bullets:
    - "Décrit le processus de collecte des données"

  - enable: false
    name : ""
    price: "CSV"
    content : "_Fichier texte de valeurs séparées par des virgules_"
    link : "#"
    services:
    - Ouvert et flexible
    - Adapté au contrôle de version (Git)
    - Pas d'étiquettes de colonne<br>(limitation technique)
    # - UTF-8 encoded; double quote character (`"`) as string separator

  - enable: false
    name : ""
    price: "ZSAV"
    content : "_Fichier de données **SPSS** compressé_"
    link : "#"
    services:
    - Propriétaire
    - Convient aux utilisateurs de SPSS
    - Noms de colonnes raccourcis à 32 caractères<br>(limitation technique)

  - enable: false
    name : ""
    price: "DTA"
    content : "_Fichier de données **Stata**_"
    link : "#"
    services:
    - Propriétaire
    - Convient aux utilisateurs de Stata
    - Noms de colonnes raccourcis à 32 caractères<br>(limitation technique)

############################ Contact ###########################
cta:
  enable: true
  # content comes from "_index.md"
---
