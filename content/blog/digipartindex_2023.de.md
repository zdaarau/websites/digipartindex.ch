---
title: "Digitale Partizipation – es braucht die Zusammenarbeit der Kantone"
date: 2024-01-15T14:00:00+01:00
author: [
  "Uwe Serdült",
  "Gabriel Hofmann",
  "Marine Benli-Trichet",
  "Stefan Kalberer",
  "Jonas Wüthrich",
  "Costa Vayenas",
  "Jean-Patrick Villeneuve",
  "Anna Picco-Schwendener"
]
image: "images/blog/dpi_map_2023.png"
bg_image: ""
categories: ["reports"]
tags: ["Civic Tech", "Democracy", "Participation", "Federalism", "Switzerland"]
description: "DigiPartindex-Bericht 2023"
draft: false
type: "post"
---

Der DigiPart-Index erfasst auf einer Skala von 0 bis 100, inwiefern es in den Kantonen der Schweiz möglich ist, sich digital an politischen Prozessen zu beteiligen. Im dritten Jahr seiner Erhebung bleibt der Durchschnittswert über alle Kantone hinweg relativ tief. Zum dritten Mal in Folge unterscheiden sich Spitzenkanton und Schlusslicht um Faktor zehn. Während sich in den Kantonen in der Schlussgruppe in den vergangenen drei Jahren in Sachen digitale Teilhabe nur wenig getan hat, kam es in der Spitzengruppe dieses Jahr zu einer weiteren Steigerung. Das liegt unter anderem an der Wiedereinführung von e-Voting.

Digitale Formen der politischen Teilhabe wie e-Voting oder Online-Umfragen ergänzen schweizweit zunehmend herkömmlichen Formen wie Wahlzettel und gedrucktes Formular. Das entspricht der digitalen Lebenswelt immer breiterer Bevölkerungskreise. Ein interdisziplinäres Forschungsteam erhebt deshalb für die Schweiz einen Index, der digitale politische Partizipation erfasst und die digitalen Mitwirkungsmöglichkeiten in den Kantonen vergleichbar abbildet. Der Index wird in einem standardisierten Wertebereich erfasst. Die Werte für den DigiPart-Index Schweiz reichen von 0 bis 100 Punkten und umfassen die Dimensionen «Meinungsbildung», «Mitwirkung» und «Entscheiden».

### Zunehmende Unterschiede zwischen den Kantonen

Auch im dritten Jahr der Erhebung des DigiPart-Index sind die Unterschiede zwischen den Kantonen beträchtlich und haben sogar leicht zugenommen. Erzielt wurden minimal 6 bis maximal 58 Punkte. Der Mittelwert beträgt 33 Punkte. Tendenziell finden sich kleinere ländliche Kantone in der Schlussgruppe und bieten nur sehr beschränkt Instrumente für die digitale politische Partizipation. Aber auch in der Spitzengruppe gibt es Spielraum für weitere Entwicklung, besonders im Bereich der e-Anliegen, wo es momentan nur private Plattformen wie beispielsweise «petitio» oder «weCollect» gibt.

Während die Spitzenkantone im hohen mittleren Wertebereich des DigiPart-Index sind, ist es in den Kantonen der Schlussgruppe in den vergangenen drei Jahren nur zu wenig Veränderung gekommen. Angesichts solch grosser Unterschiede ist Vorsicht geboten, dass die kleineren und an Ressourcen schwächeren Kantone nicht abgehängt werden. Beispielsweise, indem die (technische) Zusammenarbeit zwischen den Kantonen verstärkt wird und so kleinere Kantone vom Know-how der grösseren Kantone profitieren können. Ausser im Bereich «eAnliegen» sind denn auch bereits technische Lösungen vorhanden in den einzelnen Kantonen. Durch gegenseitiges Lernen könnten diese auch in anderen Kantonen zur Anwendung kommen. In der kleinräumigen und mehrsprachigen Schweiz ist eine solche Zusammenarbeit vor allem auch mit Blick auf die Zukunft relevant. Das zeigt die rasante Entwicklung neuer Software wie etwa ChatGPT. Derartige Entwicklungen werden das Feld der politischen Teilhabe in digitaler Form beeinflussen.

Der Index wird jährlich aufdatiert. Der volle Bericht steht nachfolgend zum Download bereit, der Datensatz ist [**hier**](/de/data) abrufbar.

[{{< picture path="images/thumbnails/DigiPartIndex_2023_de" alt="Bericht DigiPartIndex 2023" >}}](/docs/DigiPartIndex_2023_de.pdf)
