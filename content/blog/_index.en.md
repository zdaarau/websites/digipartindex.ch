---
title: "Reports"
description: "Annual reports on the DigiPartIndex"
draft: false
bg_image: "images/background/unsplash-Oaqk7qqNh_c-edited-2048w"
menu:
  main:
    name: "Reports"
    identifier: "blog"
    weight: 3
  footer:
    name: "Reports"
    identifier: "blog"
    weight: 3
---
