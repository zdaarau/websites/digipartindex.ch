---
title: "Berichte"
description: "Jährliche Berichte zum DigiPartIndex"
draft: false
bg_image: "images/background/unsplash-Oaqk7qqNh_c-edited-2048w"
menu:
  main:
    name: "Berichte"
    identifier: "blog"
    weight: 3
  footer:
    name: "Berichte"
    identifier: "blog"
    weight: 3
---
