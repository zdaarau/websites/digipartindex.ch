---
title: "Partecipazione digitale – c’è bisogno di collaborazione tra i cantoni"
date: 2024-01-15T14:00:00+01:00
author: [
  "Uwe Serdült",
  "Gabriel Hofmann",
  "Marine Benli-Trichet",
  "Stefan Kalberer",
  "Jonas Wüthrich",
  "Costa Vayenas",
  "Jean-Patrick Villeneuve",
  "Anna Picco-Schwendener"
]
image: "images/blog/dpi_map_2023.png"
bg_image: ""
categories: ["reports"]
tags: ["Civic Tech", "Democracy", "Participation", "Federalism", "Switzerland"]
description: "Rapporto DigiPartindex 2023"
draft: false
type: "post"
---

Il DigiPart-Index misura su una scala da 0 a 100 le possibilità di partecipazione digitale ai processi politici nei Cantoni svizzeri. Nel terzo anno di rilevazione, il punteggio medio di tutti i Cantoni rimane relativamente basso. Per la terza volta consecutiva, il Cantone in testa alla classifica ha un punteggio dieci volte più alto di quello in coda alla classifica. Se negli ultimi tre anni la partecipazione digitale è rimasta pressoché invariata nei Cantoni della fascia bassa, quest’anno si è registrato un ulteriore incremento nel gruppo della fascia alta - ciò è dovuto, in parte, alla reintroduzione del voto elettronico. 

In tutta la Svizzera le forme digitali di partecipazione politica, come il voto elettronico o i sondaggi online, integrano sempre di più le forme tradizionali come le schede elettorali e i moduli stampati. Questa tendenza coincide con lo stile di vita digitale sempre più diffuso tra le persone. Di conseguenza, un gruppo interdisciplinare di ricercatori e ricercatrici ha sviluppato un indice specifico per la Svizzera, volto a misurare la partecipazione politica digitale e a fornire un quadro comparativo delle opportunità di coinvolgimento digitale nei diversi Cantoni. L'indice viene registrato attraverso un intervallo di valori standardizzati. I valori del DigiPart-Index Svizzera vanno da 0 a 100 punti e comprendono i campi “formazione dell’opinione”, “partecipazione” e “processo decisionale”.

### Crescono le differenze tra i Cantoni

Nel terzo anno del sondaggio DigiPart-Index, le differenze tra i Cantoni sono ancora notevoli e sono addirittura leggermente aumentate, toccando un minimo di 6 e raggiungendo un massimo di 58 punti. Il valore medio si attesta a 33 punti. I Cantoni rurali, più piccoli tendono a collocarsi nel gruppo con i punteggi più bassi, e offrono solo strumenti molto limitati per la partecipazione politica digitale. Tuttavia, anche nel gruppo in testa c’è spazio per ulteriori sviluppi, soprattutto nell’ambito delle richieste elettroniche, dove attualmente esistono solo piattaforme private come « petitio » o « weCollect ». 

Mentre i Cantoni migliori si collocano nella fascia medio-alta del DigiPart-Index, negli ultimi tre anni si sono registrati pochi cambiamenti nei Cantoni della fascia inferiore. Alla luce di queste grandi differenze, è necessaria una certa cautela per garantire che i Cantoni più piccoli e con meno risorse non rimangano indietro, rafforzando per esempio la cooperazione (tecnica), in modo che quelli più piccoli possano beneficiare delle competenze di quelli più grandi. Escludendo l’ambito delle “richieste elettroniche”, le soluzioni tecniche sono già disponibili in singoli Cantoni, e, grazie all'apprendimento reciproco, queste soluzioni potrebbero essere utilizzate su più ampia scala. In un paese piccolo e multilingue come la Svizzera, tale cooperazione è particolarmente importante in vista del futuro. Il rapido sviluppo di nuovi strumenti come ChatGPT, influenzerà anche il campo della partecipazione politica digitale.

L'indice viene aggiornato annualmente. Il rapporto completo è disponibile per il download (in tedesco) qui sotto, il set di dati è disponibile [**qui**](/it/data).

[{{< picture path="images/thumbnails/DigiPartIndex_2023_de" alt="Bericht DigiPartIndex 2023" >}}](/docs/DigiPartIndex_2023_de.pdf)
