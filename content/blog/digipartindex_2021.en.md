---
title: "Considerable differences between the cantons"
date: 2021-10-25T21:00:00+02:00
author: [ "Uwe Serdült", "Costa Vayenas", "Gabriel Hofmann", "Herveline Du Clary" ]
image: "images/blog/digipartindex_2021.png"
bg_image: ""
categories: ["reports"]
tags: ["Civic Tech", "Democracy"]
description: "DigiPartindex Report 2021"
draft: false
type: "post"
---

The first edition of the **Index of Digital Political Participation in Switzerland (DigiPartIndex)** records the extent to which it is possible to participate digitally in the political process in Swiss cantons on a scale of 0 to 100. Digital political participation is measured with the help of seven indicators covering three dimensions: opinion-forming, participation and decision-making. The ranking of the cantons shows that there is still a lot of room for improvement. The highest score is achieved by the Canton of Geneva with 55 points. The index was launched by the Centre for democracy studies Aarau (ZDA) and the Procivis Think Tank, supported by the Mercator Foundation Switzerland.

Digital political participation increasingly complements analogue forms of political participation. Elements of the political process such as dialogue, consultation, participation as well as voting have received a further digital boost in the current pandemic. Because they reflect the new digital experiences of ever broader sections of the population, using digital means to participate in the political process will play an increasingly important role in the future. The Centre for democracy studies Aarau and the Procivis Think Tank are, therefore, introducing an index for Switzerland that measures digital political participation and maps cantons in a comparable way. Such a monitoring system does not yet exist for Switzerland.

Digital political participation is recorded with the help of an index in a standardised value range. The values for the Index of Digital Political Participation Switzerland range from 0 to 100 points. The differences between the cantons are considerable, ranging from a minimum of 6 and a maximum of 55 points. The mean value is 31 points. The ranking tends to be led by cantons with a large population and greater financial resources. However, even the cantons at the top of the index still have room for significant improvement in all areas.

The index measures three dimensions of digital political participation. The first dimension reflects how political decision-making in democracies is preceded by an opinion-forming phase. It covers tools for e-deliberation, digital political education and e-transparency. The second dimension, participation, maps the institutionalised exchange between government agencies and civil society. The two components, e-consultation and e-requests, are surveyed for this purpose. Thirdly, in addition to public debate and an exchange between the state and society, tools can also be used to enable the act of voting and electing digitally. To this end, the foundations must be laid in the form of electronic identification, i.e. an e-ID, so that it can then be used for e-voting and e-collecting, among other things. Bonus or penalty points can be awarded for additional criteria such as use, user group, user-friendliness and diversity.

The index is updated annually. The full report is available for download below, and the dataset is available [**here**](/en/data).

[{{< picture path="images/thumbnails/DigiPartIndex_2021_en" alt="Report DigiPartIndex 2021" >}}](/docs/DigiPartIndex_2021_en.pdf)
