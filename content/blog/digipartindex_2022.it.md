---
title: "La fascia media ha recuperato terreno"
date: 2022-10-10T07:00:00+02:00
author: ["Gabriel Hofmann", "Uwe Serdült", "Costa Vayenas", "Marine Benli-Trichet", "Jean-Patrick Villeneuve", "Anna Picco-Schwendener", "Leonardo Colosante"]
image: "images/blog/dpi_ordered_21_22.png"
bg_image: ""
categories: ["reports"]
tags: ["Civic Tech", "Democracy", "Participation", "Federalism", "Switzerland"]
description: "Rapporto DigiPartindex 2022"
draft: false
type: "post"
---

L'indice DigiPart (DPI) rileva le possibilità di partecipazione digitale ai processi politici nei cantoni svizzeri su una scala da 0 a 100. Nel secondo anno di indagine, il punteggio medio di tutti i cantoni è ancora relativamente basso, ma è passato da 31 a 33 punti. La classifica dei cantoni mostra che la partecipazione digitale ai processi politici può essere migliorata in tutti i cantoni. Allo stesso tempo, i cantoni della fascia medio-bassa stanno recuperando terreno e competono con i cantoni della fascia alta. Ciò è dovuto, da un lato, a piccoli e graduali miglioramenti in tutte le aree coperte dall'indice e, dall'altro, a una certa instabilità nell'offerta e nell'utilizzo di strumenti nella dimensione "partecipazione".

La partecipazione politica digitale integra sempre di più le forme analogiche di partecipazione politica. Elementi del processo politico come il dialogo, la consultazione, la partecipazione, il voto e l'elezione nello spazio digitale, stanno diventando man mano più importanti. Questo perché corrispondono ai nuovi modi di vita e alle esperienze digitali di fasce sempre più ampie della popolazione. Il Centro per la Democrazia di Aarau (ZDA), il Think Tank Procivis così come l'Istituto di comunicazione e politiche pubbliche e l'Istituto di tecnologie digitali per la comunicazione dell'Università della Svizzera italiana stanno quindi stilando un indice per la Svizzera che registri la partecipazione politica digitale e mappi i cantoni in modo che siano comparabili fra loro.

La partecipazione politica digitale viene misurata con l'aiuto di un indice, in un intervallo di valori standardizzato. I valori del DigiPart Index Svizzero vanno da 0 a 100 punti e comprendono le dimensioni "formazione dell'opinione ", "partecipazione" e "processo decisionale". Le differenze tra i cantoni sono notevoli anche nel 2022. Sono stati raggiunti da un minimo di 5 a un massimo di 56 punti. Il valore medio è di 33 punti. I cantoni finanziariamente più forti tendono a guidare la classifica. I cantoni con una struttura demografica più anziana offrono generalmente meno possibilità di partecipazione digitale. Tuttavia, anche i cantoni più alti in classifica hanno ancora un buon margine di miglioramento in tutti i settori.

Rispetto all'anno precedente, si sono registrate fluttuazioni soprattutto nella dimensione della "partecipazione". Questa instabilità può essere osservata sia nell’ambito della "consultazione elettronica" che in quello della "richiesta elettronica" ed è dovuta principalmente all'interruzione dei test pilota o al minore utilizzo delle piattaforme corrispondenti da parte della società civile. Nella dimensione della "formazione delle opinioni", in alcuni cantoni sono stati osservati piccoli miglioramenti negli ambiti della "deliberazione elettronica", "formazione politica digitale" e "trasparenza elettronica". Infine, anche nella dimensione del "processo decisionale" - in particolare nell’ambito della "identificazione elettronica (eID)" - sono stati registrati piccoli miglioramenti per pochi cantoni. Nulla è cambiato nell’ambito del "voto elettronico".


L'indice viene aggiornato annualmente. Il rapporto completo è disponibile per il download (in tedesco) qui sotto, il set di dati è disponibile [**qui**](/it/data).

[{{< picture path="images/thumbnails/DigiPartIndex_2022_de" alt="Bericht DigiPartIndex 2022" >}}](/docs/DigiPartIndex_2022_de.pdf)
