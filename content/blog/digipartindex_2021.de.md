---
title: "Beträchtliche Unterschiede zwischen den Kantonen"
date: 2021-10-25T21:00:00+02:00
author: [ "Uwe Serdült", "Costa Vayenas", "Gabriel Hofmann", "Herveline Du Clary" ]
image: "images/blog/digipartindex_2021.png"
bg_image: ""
categories: ["reports"]
tags: ["Civic Tech", "Democracy"]
description: "DigiPartindex-Bericht 2021"
draft: false
type: "post"
---

Die erste Ausgabe des **Index digitale politische Partizipation in der Schweiz (DigiPartIndex)** erfasst auf einer Skala von 0 bis 100, inwiefern es in den Kantonen der Schweiz möglich ist, sich digital am politischen Prozess zu beteiligen. Gemessen wird digitale politische Partizipation mit Hilfe von insgesamt sieben Indikatoren für die drei Dimensionen *Meinungsbildung*, *Mitwirkung* und *Entscheidung*. Im Ranking der Kantone wird ersichtlich, dass gegen oben noch viel Luft ist. Den höchsten Wert erreicht der Kanton Genf mit 55 Punkten. Lanciert wird der Index vom Zentrum für Demokratie Aarau (ZDA) und dem Think Tank von Procivis, unterstützt von der Stiftung Mercator Schweiz.

Digitale politische Partizipation ergänzt zunehmend analoge Formen politischer Beteiligung. Elemente des politischen Prozesses wie Dialog, Konsultation, Beteiligung sowie Abstimmen und Wählen im digitalen Raum haben gerade in Zeiten von COVID-19 wieder einen Schub erhalten. Weil sie den neuen digitalen Lebenswelten und -erfahrungen immer breiterer Bevölkerungskreise entsprechen, werden sie in Zukunft eine immer wichtigere Rolle spielen. Das Zentrum für Demokratie Aarau und der Procivis Think Tank führen deshalb für die Schweiz einen Index ein, der digitale politische Partizipation erfasst und Kantone vergleichbar abbildet. Ein solches Monitoring gibt es für die Schweiz noch nicht.

Digitale politische Partizipation wird mit Hilfe eines Index in einem standardisierten Wertebereich erfasst. Die Werte für den DigiPartIndex Schweiz reichen von 0 bis 100 Punkten. Die Unterschiede zwischen den Kantonen sind beträchtlich. Erzielt wurden minimal 6 bis maximal 55 Punkte. Der Mittelwert beträgt 31 Punkte. Tendenziell führen bevölkerungsreiche und finanzstärkere Kantone das Ranking an. Auch die an der Spitze liegenden Kantone können sich jedoch noch in allen Bereichen deutlich verbessern.

Der Index misst drei Dimensionen der digitalen politischen Partizipation. In der ersten Dimension wird abgebildet, dass politischen Entscheiden in Demokratien eine Phase der Meinungsbildung vorangeht. Erfasst werden dabei Tools für E-Deliberation, digitale politische Bildung und E-Transparenz. Mitwirkung als zweite Dimension bildet den institutionalisierten Austausch zwischen staatlichen Stellen und der Zivilgesellschaft ab. Hierzu erhoben werden die beiden Komponenten E-Konsultation und E-Anliegen. Neben der öffentlichen Debatte und einem Austausch zwischen Staat und Gesellschaft können drittens auch Werkzeuge zum Einsatz kommen, die den Akt des Abstimmens und Wählens digital ermöglichen. Dazu müssen die Grundlagen in Form einer elektronischen Identifikation, also einer E-ID, geschaffen werden, um dann unter anderem auch für das E-Voting sowie E-Collecting zum Einsatz kommen zu können. Für zusätzliche Kriterien wie Gebrauch, Nutzerkreis, Benutzerfreundlichkeit und Vielfalt können Bonus- oder Maluspunkte vergeben werden.

Der Index wird jährlich aufdatiert. Der volle Bericht steht nachfolgend zum Download, der Datensatz ist [**hier**](/de/data) abrufbar.

[{{< picture path="images/thumbnails/DigiPartIndex_2021_de" alt="Bericht DigiPartIndex 2021" >}}](/docs/DigiPartIndex_2021_de.pdf)
