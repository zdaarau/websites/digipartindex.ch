---
title: "Le milieu de classement a rattrapé son retard"
date: 2022-10-10T07:00:00+02:00
author: ["Gabriel Hofmann", "Uwe Serdült", "Costa Vayenas", "Marine Benli-Trichet", "Jean-Patrick Villeneuve", "Anna Picco-Schwendener", "Leonardo Colosante"]
image: "images/blog/dpi_ordered_21_22.png"
bg_image: ""
categories: ["reports"]
tags: ["Civic Tech", "Democracy", "Participation", "Federalism", "Switzerland"]
description: "Rapport DigiPartindex 2022"
draft: false
type: "post"
---

Le DigiPartIndex (DPI) recense, sur une échelle de 0 à 100, des données sur les possibilités de participer numériquement aux processus politiques dans chaque canton suisse. À l'issue de sa deuxième année d'enquête, la valeur moyenne de tous les cantons demeure relativement basse, bien qu'elle soit passée de 31 à 33 points. Le classement général montre qu'il y a encore une grande marge de progression possible en termes de participation politique digitale et ce dans tous les cantons. Les cantons précédemment situés dans la moyenne basse du classement rattrapent leur retard et rivalisent avec les cantons en tête de classement. Cela est dû, d'une part, à des améliorations progressives dans tous les domaines couverts par l'indice et, d'autre part, à une certaine instabilité concernant l'offre et l'utilisation d'outils relatifs à la dimension « participation ».

La participation politique digitale complète de plus en plus les formes analogiques de participation politique. Les éléments du processus politique tels que le dialogue, la consultation, la participation ainsi que le vote et l'élection dans l'espace numérique prennent de plus en plus d'importance. En effet, ils correspondent aux nouveaux environnements et aux expériences de vie numériques d'un segment grandissant de la population. C'est pourquoi le Centre pour la démocratie d'Aarau (ZDA), le think tank Procivis et l'Institut Communication et Politiques publiques (ICPP) et l'Institut des Technologies numériques de la Communication (ITDxC) de l'Università della Svizzera italiana (USI) présentent un indice suisse qui évalue la participation politique digitale, et cartographie les cantons de manière comparée.

La participation politique digitale est évaluée à l'aide d'un indice dans une plage de valeurs standardisée. Les valeurs du DigiPartIndex vont de 0 à 100 points et comprennent les dimensions « formation de l'opinion », « participation » et « prise de décision ». Les différences entre les cantons restent considérables en 2022. Un minimum de 5 points et un maximum de 56 points ont été obtenus. La valeur moyenne est de 33 points. Les cantons les plus solides sur le plan financier se retrouvent généralement en tête du classement. Les cantons dont la structure démographique est plus vieillissante ont tendance à offrir moins de possibilités de participation digitale. Cependant, même les cantons les plus performants peuvent encore s'améliorer dans tous les domaines analysés.

Par rapport à l'année précédente, c'est principalement sur la dimension « participation » que des fluctuations sont observées. Cette instabilité est aussi bien visible dans le domaine de « l'e-Consultation » que dans celui des « e-Requêtes » et s'explique notamment par l'arrêt des essais pilotes ainsi qu'une utilisation plus faible des plateformes correspondantes par la société civile. Pour la dimension « formation de l'opinion », de petites améliorations ont été observées dans certains cantons dans les domaines de la « e- Délibération », de la « e-Éducation civique » et de la « e-Transparence ». Enfin, de petites améliorations ont également été enregistrées pour quelques cantons sur la dimension « prise de décision » - surtout dans le domaine de « l'e-ID ». Dans le domaine du « e-Voting », aucune variation n'a été observée.

L'indice est mis à jour chaque année. Le rapport (en allemand) peut être téléchargé ci-après, quant à l'ensemble des données, elles sont disponibles [**ici**](/fr/data).

[{{< picture path="images/thumbnails/DigiPartIndex_2022_de" alt="Bericht DigiPartIndex 2022" >}}](/docs/DigiPartIndex_2022_de.pdf)
