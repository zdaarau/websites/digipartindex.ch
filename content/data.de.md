---
title: "Daten"
description: "Die Daten und Flussdiagramme hinter dem DigiPartIndex"
subtitle: ""
bg_image: "images/background/unsplash-trYl7JYATH0-edited-2048w"
layout: "data"
draft: false
menu:
  main:
    name: "Daten"
    weight: 5
  footer:
    name: "Daten"
    weight: 5

################################ Datasets ################################
datasets:
  enable : true
  items:
  - enable: true
    head : ""
    title: "Datensatz"
    content : "_XLSX-Datei_"
    link : "/download/DigiPartIndex.xlsx"
    bullets:
    - "Enthält 2 Tabellen:"
    - "Datensatz plus Metadaten"

  - enable: true
    head : ""
    title: "Fluss­diagramme"
    content : "_PDF-Datei_"
    link : "/download/Flow_Charts_Data_Collection.pdf"
    bullets:
    - "Beschreibt den Ablauf der Datenerhebung"

  - enable: false
    head : ""
    title: "CSV"
    content : "_Textdatei mit Komma-separierten Werten_"
    link : "/datasets/download/digipartindex_2021.csv"
    bullets:
    - "Enthält den Hauptdatensatz"
    - "Die Metadaten finden sich [hier](/datasets/digipartindex_metadata.csv)"
#    - Offen und flexibel
#    - Geeignet zur Versionskontrolle (Git)
#    - Keine Spalten-Labels<br>(Technische Limitierung)
#    - UTF-8-enkodiert<br>doppelte Anführungszeichen (`"`) als Zeichenketten-Trenner

  - enable: false
    head : ""
    title: "ZSAV"
    content : "_Komprimierte **SPSS**-Datei_"
    link : "#"
    bullets:
    - Proprietär
    - Geeignet für SPSS-Nutzer:innen
    - Spaltennamen gekürzt auf 32 Zeichen<br>(Technische Limitierung)

  - enable: false
    head : ""
    title: "DTA"
    content : "_**Stata**-Datei_"
    link : "#"
    bullets:
    - Proprietär
    - Geeignet für Stata-Nutzer:innen
    - Spaltennamen gekürzt auf 32 Zeichen<br>(Technische Limitierung)

############################ Contact ###########################
cta:
  enable: true
  # content comes from "_index.md"
---
