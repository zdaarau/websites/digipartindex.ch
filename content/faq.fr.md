+++
title = "Questions fréquentes"
subtitle = ""
description = "Questions et réponses sur le DigiPartIndex"
bg_image = "images/background/unsplash-8xAA0f9yQnE-edited-2048w"
layout = "faq"
draft = true
[menu.main]
  name = "Questions fréquentes"
  weight = 5
[menu.footer]
  name = "Questions fréquentes"
  weight = 5
+++

#### 1. Qui finance ce projet ?

...

#### 2. Comment les données sont-elles collectées ?

...

#### 3. Existe-t-il d'autres indices dans le domaine de la participation politique numérique ?

...
