---
menu:
  main:
    name: "Accueil"
    weight: 1

############################### Banner ##############################
banner:
  enable: true
  bg_image: "images/background/unsplash-B4lknSRZwPM-edited-2048w.jpg"
  bg_overlay: true
  title: "Index de participation politique digitale en Suisse"
  content: ""
  button:
    enable: false
    label: ""
    link: ""

############################# About #################################
about:
  enable: true
  title: "Le concept de \"participation politique digitale\""
  description: ""
  content: "…doit être saisi dans une gamme de valeurs normalisées grâce à un index. Nous nous concentrerons sur les outils digitaux qui permettent la participation politique effective. Nous commencerons par couvrir tous les cantons suisses, mais nous pourrions étendre l'index à certaines villes et municipalités, jusqu'à peut-être évaluer le niveau fédéral.


    La participation politique digitale est un complément toujours plus courant des formes analogiques de participation politique. Des éléments du processus politique tels que le dialogue, la consultation, la participation ou encore la prise de décision dans l'espace digital ont connu un nouvel élan, notamment en temps de covid-19. Correspondant au nouveau monde digital et aux expériences des jeunes générations, ils continueront à jouer un rôle important à l'avenir. La gouvernance numérique, qui s'inscrit dans le contexte de \"villes intelligentes\", et les outils qui lui sont propres se répandent dans le monde entier. La situation est particulièrement dynamique au niveau infranational ainsi que dans les grandes villes (voir par exemple la diffusion croissante de la plateforme de participation open-source [CONSUL](https://consulproject.org/))."
  image:
    src: "images/logos/digipartindex.svg"
    link: "project"
  button:
    enable: true
    label: "Plus sur le projet"
    link: "project"

######################### Data ###############################
data:
  enable: true
  bg_image: "images/background/unsplash-HOrhCnQsxnQ-2048w.jpg"
  title: "Données"
  content: "Les données sur lesquelles l'indice est basé sont librement disponibles pour le public intéressé."
  button:
    enable: true
    label: "Vers les données"
    link: "data"

############################# FAQ ############################
faq:
  enable: false
  title: "Un recueil des questions les plus fréquemment posées"
  content: "..."
  button:
    enable: true
    label: "Questions fréquentes"
    link: "faq"

############################ Contact ###########################
cta:
  enable: true
  bg_image: "images/background/unsplash-xbrMwBgYUHY-2048w.jpg"
  title: "Données ouvertes, questions ouvertes ?"
  content: "Travailler avec des données inconnues peut sembler laborieux. Nous sommes heureux de vous aider pour tout problème. N'hésitez pas à…"
  button:
    enable: true
    label: "Nous contacter"
    link: "contact"

############################# Logos ###############################
logos:
  enable: true
  title: "Réalisé par"
---
