---
title: "Dati"
description: "I dati e i diagrammi di flusso alla base del DigiPartIndex"
subtitle: ""
bg_image: "images/background/unsplash-trYl7JYATH0-edited-2048w"
layout: "data"
draft: false
menu:
  main:
    name: "Dati"
    weight: 5
  footer:
    name: "Dati"
    weight: 5

################################ Datasets ################################
datasets:
  enable : true
  items:
  - enable: true
    head : ""
    title: "Set di dati"
    content : "_XLSX file_"
    link : "/download/DigiPartIndex.xlsx"
    bullets:
    - "Contiene 2 tabelle:"
    - "Set di dati più metadati"

  - enable: true
    head : ""
    title: "Diagrammi di flusso"
    content : "_PDF-Datei_"
    link : "/download/Flow_Charts_Data_Collection.pdf"
    bullets:
    - "Descrive il processo di raccolta dei dati"

  - enable: false
    name : ""
    price: "CSV"
    content : "_File di testo con valori separati da virgola_"
    link : "#"
    services:
    - Open and flexible
    - Adatto al controllo di versione (Git)
    - Nessuna etichetta di colonna<br>(limitazione tecnica)
    # - UTF-8 encoded; double quote character (`"`) as string separator

  - enable: false
    name : ""
    price: "ZSAV"
    content : "_File di dati **SPSS** compresso_"
    link : "#"
    services:
    - Proprietario
    - Adatto agli utenti di SPSS
    - Nomi delle colonne abbreviati a 32 caratteri<br>(limitazione tecnica)

  - enable: false
    name : ""
    price: "DTA"
    content : "_File di dati di **Stata**_"
    link : "#"
    services:
    - Proprietario
    - Adatto agli utenti di Stata
    - Nomi delle colonne abbreviati a 32 caratteri<br>(limitazione tecnica)

############################ Contact ###########################
cta:
  enable: true
  # content comes from "_index.md"
---
