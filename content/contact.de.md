---
title: "Kontakt"
description: "Kontaktieren Sie uns bei Fragen rund um den DigiPartIndex"
bg_image: "images/background/unsplash-L5HG3CH_pgc-edited-2048w"
layout: "contact"
draft: false
menu:
  main:
    name: "Kontakt"
    weight: 7
  footer:
    name: "Kontakt"
    weight: 7
---
