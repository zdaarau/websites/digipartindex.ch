---
menu:
  main:
    name: "Startseite"
    weight: 1

############################### Banner ##############################
banner:
  enable: true
  bg_image: "images/background/unsplash-B4lknSRZwPM-edited-2048w"
  bg_overlay: true
  title: "Index digitale politische Partizipation in der Schweiz"
  content: ""
  button:
    enable: false
    label: ""
    link: ""

############################# About #################################
about:
  enable: true
  title: "Das Konzept «digitale politische Partizipation»"
  description: ""
  content: "…soll mit Hilfe eines Index in einem standardisierten Wertebereich erfasst werden. Der Hauptakzent soll auf denjenigen digitalen Instrumenten liegen, die eine effektive politische Partizipation ermöglichen. Wir werden zunächst alle Schweizer Kantone abdecken, wobei aber die Möglichkeit besteht, den Index auf ausgewählte Städte und Gemeinden zu erweitern und auch die Bundesebene miteinzubeziehen.


    Digitale politische Partizipation komplementiert zunehmend analoge Formen politischer Beteiligung. Elemente des politischen Prozesses wie Dialog, Konsultation, Beteiligung sowie Entscheidungsfindung im digitalen Raum haben gerade in Zeiten von COVID-19 wieder einen Schub erhalten. Weil sie den neuen digitalen Lebenswelten und -erfahrungen der jüngeren Generationen entsprechen, werden sie auch in Zukunft eine wichtige Rolle spielen. Im Rahmen von «Smart Cities» stattfindende digitale Governance mit entsprechenden Tools verbreitet sich in einem gegenseitigen Austausch weltweit. Besonders dynamisch ist die Situation auf subnationaler Ebene und in grossen Städten (siehe etwa die zunehmende Verbreitung der Open-Source-Partizipations-Plattform [CONSUL](https://consulproject.org/))."
  image:
    src: "images/logos/digipartindex.svg"
    link: "project"
  button:
    enable: true
    label: "Mehr zum Projekt"
    link: "project"

######################### Data ###############################
data:
  enable: true
  bg_image: "images/background/unsplash-HOrhCnQsxnQ-2048w"
  title: "Daten"
  content: "Die dem Index zugrunde liegenden Daten stehen der interessierten Öffentlichkeit frei zur Verfügung."
  button:
    enable: true
    label: "Zu den Daten"
    link: "data"

############################# FAQ ############################
faq:
  enable: false
  title: "Eine Sammlung der häufigsten Fragen"
  content: "..."
  button:
    enable: true
    label: "Häufige Fragen"
    link: "faq"

############################ Contact ###########################
cta:
  enable: true
  bg_image: "images/background/unsplash-xbrMwBgYUHY-2048w"
  title: "Offene Daten, offene Fragen?"
  content: "Die Arbeit mit unbekannten Daten kann anfänglich an den Kräften zehren. Wir helfen bei Problemen gerne – zögern Sie nicht, uns zu..."
  button:
    enable: true
    label: "kontaktieren"
    link: "contact"

############################# Logos ###############################
logos:
  enable: true
  title: "Realisiert durch"
---
