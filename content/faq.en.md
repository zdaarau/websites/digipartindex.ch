+++
title = "Frequently Asked Questions"
subtitle = ""
description = "Questions and answers about the DigiPartIndex"
bg_image = "images/background/unsplash-8xAA0f9yQnE-edited-2048w"
layout = "faq"
draft = true
[menu.main]
  name = "FAQ"
  weight = 5
[menu.footer]
  name = "FAQ"
  weight = 5
+++

#### 1. Who is funding this project?

...

#### 2. How is the data collected?

...

#### 3. Are there other indices in the field of digital political participation?

...
