---
menu:
  main:
    name: "Pagina iniziale"
    weight: 1

############################### Banner ##############################
banner:
  enable: true
  bg_image: "images/background/unsplash-B4lknSRZwPM-edited-2048w.jpg"
  bg_overlay: true
  title: "Indice di partecipazione politica digitale in Svizzera"
  content: ""
  button:
    enable: false
    label: ""
    link: ""

############################# About #################################
about:
  enable: true
  title: "Il concetto di \"partecipazione politica digitale\""
  description: ""
  content: "…deve essere inserito, con l'aiuto di un indice, in una scala di valori standardizzati. Ci concentreremo sugli strumenti digitali che permettono un'effettiva partecipazione politica. Inizialmente, ci occuperemo di tutti i cantoni svizzeri, ma c'è la possibilità di estendere l'indice a città e comuni in particolare e, probabilmente, di includere anche il territorio federale.


    La partecipazione politica digitale completa sempre più le forme analogiche della partecipazione politica. Elementi del processo politico come il dialogo, la consultazione, la partecipazione e il processo decisionale nello spazio digitale hanno ricevuto un nuovo impulso soprattutto in tempi di COVID-19. Poiché corrispondono ai nuovi mondi digitali e alle esperienze delle giovani generazioni, continueranno ad avere un ruolo importante in futuro. La governance digitale, che si svolge nel contesto delle \"città intelligenti\" con gli strumenti corrispondenti, si sta diffondendo in tutto il mondo. La situazione è particolarmente dinamica a livello sub-nazionale così come nelle grandi città (vedi, per esempio, la crescente diffusione della piattaforma di partecipazione open-source [CONSUL](https://consulproject.org/))."
  image:
    src: "images/logos/digipartindex.svg"
    link: "project"
  button:
    enable: true
    label: "Di più sul progetto"
    link: "project"

######################### Data ###############################
data:
  enable: true
  bg_image: "images/background/unsplash-HOrhCnQsxnQ-2048w.jpg"
  title: "Dati"
  content: "I dati su cui si basa l'indice sono liberamente disponibili al pubblico interessato."
  button:
    enable: true
    label: "Verso i dati"
    link: "data"

############################# FAQ ############################
faq:
  enable: false
  title: "Una raccolta delle domande più frequenti"
  content: "..."
  button:
    enable: true
    label: "Domande frequenti"
    link: "faq"

############################ Contact ###########################
cta:
  enable: true
  bg_image: "images/background/unsplash-xbrMwBgYUHY-2048w.jpg"
  title: "Dati aperti, domande aperte?"
  content: "Lavorare con dati sconosciuti può inizialmente prosciugare le vostre forze. Siamo felici di aiutarvi per qualsiasi problema – non esitate a..."
  button:
    enable: true
    label: "contattarci"
    link: "contact"

############################# Logos ###############################
logos:
  enable: true
  title: "Realizzato da"
---
