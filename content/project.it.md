---
title: "Il progetto"
description: "Descrizione del progetto e il team dietro il DigiPartIndex"
bg_image: "images/background/unsplash-W8KTS-mhFUE-2048w.jpg"
layout: "project"
draft: false
menu:
  main:
    name: "Progetto"
    weight: 2
  footer:
    name: "Progetto"
    weight: 2

########################### project #############################
about:
  enable: true
  title: "DigiPartIndex"
  column_left: "Stiamo introducendo un indice per la Svizzera che registri la partecipazione politica digitale e fornisca un quadro chiaro per paragonare i cantoni. Un tale sistema di monitoraggio non esiste ancora in Svizzera e può servire come incentivo nel campo della partecipazione digitale in Svizzera.


Il concetto di \"partecipazione politica digitale\" deve essere inserito, con l'aiuto di un indice, in una scala di valori standardizzati. Ci concentreremo sugli strumenti digitali che permettono un'effettiva partecipazione politica. Inizialmente, ci occuperemo di tutti i cantoni svizzeri, ma c'è la possibilità di estendere l'indice a città e comuni in particolare e, probabilmente, di includere anche il territorio federale.


Cosa si intende realmente per partecipazione politica digitale? L'ONU ha definito la partecipazione digitale – quella politica – come un processo che coinvolge i cittadini nella progettazione, decisione e attuazione delle politiche attraverso l'aiuto delle tecnologie dell'informazione e della comunicazione, con l'obiettivo di rendere questo processo partecipativo, inclusivo e deliberativo.


Nel nostro progetto, ci assicuriamo che venga presa in considerazione la più ampia inclusione possibile di tutti i segmenti della popolazione."
  column_right: "L'e-partecipazione non è legata esclusivamente alla cittadinanza svizzera. Inoltre, le offerte di partecipazione politica digitale non provengono solo dalle agenzie governative (top-down), ma possono anche essere offerte dalle organizzazioni della società civile (bottom-up).


Il concetto di \"partecipazione politica digitale\" include, quindi, l'opportunità di usare le tecnologie dell'informazione e della comunicazione per far ascoltare la propria voce durante le varie fasi del ciclo politico (definizione del problema, definizione dell'agenda, formulazione e processo decisionale, implementazione, valutazione) e per influenzare decisioni legalmente vincolanti e la loro preparazione da parte delle istituzioni statali. Gli strumenti e le applicazioni digitali corrispondenti possono, ma non devono, necessariamente essere forniti dallo Stato.


In poche parole, la partecipazione politica digitale facilita, grazia all' Internet, uno scambio tra la popolazione, la società civile e le agenzie del governo nelle varie fasi del processo politico."

########################## institutions ############################
institutions:
  enable: true
  item:
    - image: "images/logos/mercator_white.svg"
      url: "https://www.stiftung-mercator.ch/"
      color: "primary"
      content: ""

    - image: "images/logos/zda_white.svg"
      url: "https://zdaarau.ch/"
      color: "primary-dark"
      content: ""

    - image: "images/logos/procivis_white.svg"
      url: "https://www.procivis.ch/think-thank/"
      color: "primary-darker"
      content: ""

########################## team ############################
team:
  enable: true
  title: "Squadra e partner"
  intro: "DigiPartIndex è finanziato dal programma [\"Digitalisierung + Gesellschaft\" della Fondazione Mercator Svizzera](https://www.stiftung-mercator.ch/stiftung/digitalisierung-gesellschaft). Il team del progetto è costituito da una collaborazione tra il [Centro per la democrazia di Aarau (ZDA)](https://zdaarau.ch/) all'Università di Zurigo e il [think tank di Procivis AG](https://www.procivis.ch/think-thank/)."
  item:
    - image: "images/people/uwe.jpg"
      name: "Uwe Serdült"
      content: "**Uwe Serdült** (leader del progetto) è un associato di ricerca presso il Centro per la democrazia di Aarau (ZDA) all'Università di Zurigo e un professore al \"e-Society Laboratory\" all'Università Ritsumeikan in Giappone. Dal 2012–2018 ha diretto il progetto su larga scala \"E-Democracy\" con sede presso lo ZDA e un progetto interdisciplinare FNS su \"Emotionality and Polarisation in New Social Media\" insieme al Prof. F. Schweitzer, ETHZ (2013–2016). Le sue attività nella scienza e nei media nel campo della democrazia digitale si possono trovare sul suo [sito personale](https://uweserdult.wordpress.com/)."

    - image: "images/people/costa.jpg"
      name: "Costa Vayenas"
      content: "**Costa Vayenas** (leader del progetto) è l'autore del libro [\"Democracy in the Digital Age\"](https://books.google.ch/books/about/Democracy_in_the_Digital_Age.html?id=IpCfDwAAQBAJ) e dirige il [think tank Procivis](https://www.procivis.ch/think-thank), che studia l'impatto delle tecnologie digitali sulla democrazia. Oltre a una vasta gamma di attività di docenza in patria e all'estero (tra cui il Parlamento europeo nel settore dell'e-government), attualmente insegna anche all'ETH di Zurigo come docente del nuovo programma post-laurea [\"Technology and Public Policy Programme\"](https://tpp.ethz.ch/)."

    - image: "images/people/gabriel.jpg"
      name: "Gabriel Hofmann"
      content: "**Gabriel Hofmann** è un assistente di ricerca presso il Centro per la democrazia di Aarau (ZDA) all'Università di Zurigo. Studia scienze politiche con particolare attenzione alla politica svizzera all'Istituto di scienze politiche di Zurigo, dove è stato anche assistente di ricerca presso la cattedra di analisi e valutazione delle politiche. Durante i suoi studi, si è occupato intensamente delle competenze civiche e dei processi decisionali."

    - image: "images/people/marine.jpg"
      name: "Marine Benli-Trichet"
      content: "**Marine Benli-Trichet** è assistente di ricerca al Centro per la democrazia di Aarau (ZDA). Dal 2018, è dottoranda presso l'Istituto di scienze politiche dell'Università di Zurigo, concentrandosi sulla digitalizzazione e la politica locale. Nella sua tesi, studia l'impatto delle tecnologie civiche sulla democrazia. Parallelamente, ha condotto una ricerca insieme al Laboratorio di Sociologia Urbana (LaSUR) del Politecnico Federale di Losanna (EPFL) nell'ambito del progetto [\"Barometro della tecnologia civica 2021\"](https://www.epfl.ch/labs/lasur/fr/barometro-della-civic-tech-2021/) project."


########################## partners ############################

  partner:
    enable: true
    title: "Soci"
    intro: "Oltre alla ZDA e al think tank Procivis, altre istituzioni sono associate al progetto. Dall'estate 2022 è stata avviata una collaborazione con l'[Università della Svizzera Italiana (USI)](https://www.usi.ch/en), tra gli altri."
    item:
      - image: "images/people/jeanpatrick.jpg"
        name: "Jean-Patrick Villeneuve"
        content: "**Jean-Patrick Villeneuve** è professore associato all'Università della Svizzera italiana (USI) di Lugano. È direttore dell'Istituto di comunicazione e politiche pubbliche e del GRIP (Public Integrity Research Group). È membro fondatore della Global Conference on Transparency Research e, fino a poco tempo fa, membro dell'Independent Expert Panel dell'Open Government Partnership. \n

        La sua ricerca si concentra sui temi della partecipazione civica, della trasparenza, della lotta alla corruzione e della responsabilità. È particolarmente interessato ai limiti, alle difficoltà e agli impatti dell'attuazione di queste iniziative di governance. Le sue ricerche sono state finanziate dal Fondo Nazionale Svizzero per la Ricerca Scientifica, dalla Rete Svizzera di Studi Internazionali, da SwissUniversities and the Social Sciences, dal Consiglio di Ricerca Umanistica del Canada e da altri. Il Prof. Villeneuve ha lavorato presso le Nazioni Unite e in diverse organizzazioni pubbliche."

      - image: "images/people/anna.jpg"
        name: "Anna Picco-Schwendener"
        content: "**Anna Picco-Schwendener**, PhD, è ricercatrice post-dottorato presso la Facoltà di Comunicazione, Cultura e Società dell'Università della Svizzera italiana (USI) dove insegna E-Government e Online Communication Design. Lavora inoltre come collaboratrice scientifica presso l'eLearning Lab dell'USI, dove è responsabile del [Centro di competenza in diritto digitale](http://www.ccdigitallaw.ch) e rappresenta l'USI all'interno dell'unità operativa del [Lugano Living Lab](https://luganolivinglab.ch). La sua tesi di dottorato verteva su \"Dimensioni sociali delle reti Wi-Fi pubbliche su larga scala: I casi di una rete wireless comunale e comunitaria\"."

########################## former teammembers ############################

  former_team:
    enable: true
    title: "Ex membri della squadra"
    item:
      - image: "images/people/herveline.jpg"
        name: "Herveline Du Clary"
        content: "**Herveline Du Clary**, con la sua formazione in studi di lingua, cultura e comunicazione, ha curato la traduzione in francese del sito web e del [rapporto 2021](/it/blog/digipartindex_2021/) fino alla fine del 2021."
      - image: "images/people/leonardo.jpg"
        name: "Leonardo Colasante"
        content: "**Leonardo Colasante** si è occupato della traduzione in italiano del [rapporto 2021](/it/blog/digipartindex_2021/) e della raccolta dei dati per il Canton Ticino dalla metà del 2022 all'inizio del 2023, parallelamente al suo Master in Public Management and Policy presso l'Università della Svizzera italiana (USI) a Lugano."

############################# contact #################################
contact:
  enable: false
  # content comes from "_index.md"
---
