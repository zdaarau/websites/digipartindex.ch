# activate renv
source("renv/activate.R")

# set opts
options(warnPartialMatchArgs = TRUE,
        warnPartialMatchAttr = TRUE,
        warnPartialMatchDollar = TRUE)

# register rlang's default global handlers
if (nzchar(system.file(package = "rlang"))) {
  
  rlang::global_handle()
  
  library(rlang,
          include.only = c("%|%", "%||%"))
}

# attach pkgs
if (nzchar(system.file(package = "magrittr"))) {
  
  library(magrittr,
          include.only = c("%>%", "%<>%", "%T>%", "%!>%", "%$%"))
}
        
# set RNG seeds
set.seed(42L)

if (nzchar(system.file(package = "htmlwidgets"))) {
  htmlwidgets::setWidgetIdSeed(seed = 42L)
}

# restore renv state from lockfile (after RStudio is launched to be able to see progress)
setHook(hookName = "rstudio.sessionInit",
        value = function(newSession) {
          
          show_progress <- nzchar(system.file(package = "cli")) && interactive()
          
          if (show_progress) {
            cli_id <- cli::cli_progress_step(msg = "Restoring renv state...",
                                             msg_done = "Restoring renv state...done!",
                                             .auto_close = FALSE)
          }
          
          renv::restore(clean = TRUE,
                        prompt = FALSE)
          
          if (show_progress) {
            cli::cli_progress_done(id = cli_id)
          }
        },
        action = "append")
